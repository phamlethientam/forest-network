import React, { Component } from 'react';
import { connect } from 'react-redux';
import ApiService from './../services/api.service';
import Header from '../components/Header';
import ProfilePostWall from '../components/ProfilePostWall';
import ListFollowers from '../components/ListFollowers';
import ListFollowing from '../components/ListFollowing';
import RightSidebar from '../components/RightSidebar';
import CreateAccount from '../components/CreateAccount'
import Payment from '../components/Payment'
import EditProfile from '../components/EditProfile'
import PropTypes from 'prop-types'
import defaultAvatar from './../img/default-avatar.png'
import {Link} from 'react-router-dom'

const mapStateToProps = (state) => {
  return {
    profile: state.profileReducer.info
  }
}

class Profile extends Component {
  static propTypes = {
    profile: PropTypes.object
  }
  constructor(props) {
    super(props)
    this.apiService = ApiService()
    this.state = {
      numNavTag: 1,
    }
  }

  handleTimeline() {
    this.setState({
      numNavTag: 1
    })
  }

  handleFollowers() {
    this.setState({
      numNavTag: 2
    })
  }

  handleFollowing() {
    this.setState({
      numNavTag: 3
    })
  }


  render() {
    let { profile } = this.props
    return (
        <div>
          <Header/>

          {/* Cover IMG */}
          <div className="headp">
            <img src="http://gg.gg/clhfc" alt=""/>
          </div>
          {/* Cover IMG */}

          {/* Menu */}
            <nav className="bottomnav">
              <div>
                {/*eslint-disable-next-line*/}
                <a data-target="mobile-demo" className="sidenav-trigger">
                  <i className="material-icons">menu</i>
                </a>
                <ul className="bottomul left hide-on-med-and-down">
                  <li>
                    <img src={profile.avatar ? profile.avatar : defaultAvatar} width="150" height="150" alt="" className="circle photoprofile" ></img>
                  </li>
                  <li onClick={this.handleTimeline.bind(this)}>
                    {/*eslint-disable-next-line*/}
                    <a>
                      <small>
                        Posts <span>Home page</span>
                      </small>
                    </a>
                  </li>
                  <li onClick={this.handleFollowers.bind(this)}>
                  {/*eslint-disable-next-line*/}
                    <a>
                      <small>
                        Followers &nbsp; <span>{profile.follower.length}</span>
                      </small>
                    </a>
                  </li>
                  <li onClick={this.handleFollowing.bind(this)}>
                  {/*eslint-disable-next-line*/}
                    <a >
                      <small>
                        Following &nbsp; <span>{profile.following.length}</span>
                      </small>
                    </a>
                  </li>
                </ul>
                <ul className="right hide-on-med-and-down">
                  <li>
                    <EditProfile/>
                  </li>
                  <li>
                    <CreateAccount/>
                  </li>
                  <li>
                    <Payment/>
                  </li>
                  <li>
                    <Link to='/transaction-history' className="waves-effect waves-light btn-small">
                        <span style={{fontFamily : "'Playfair Display', serif",color: "white"} }>Transaction History</span>
                    </Link>
                  </li>
                </ul>
              </div>
            </nav>
          <div style={{ clear: "both" }} />
          {/* Menu */}

          <div className="bgcontainers">
            <div className="row">

              {/* Info */}
              <div className="col m3">
                  {/* profil */}
                  <p className="biowrap" />
                      <h5 className="profidentity">
                          {profile ? profile.username : ''}
                      </h5>
                  <p/>
                  <p className="biowrap" style={{wordWrap:'break-word'}}>{profile ? profile.public_key : ''}</p>
                  <p className="biowrap">
                      <i className="tiny material-icons">attach_money</i> <b> Balance: </b> {profile.amount} CEL<br />
                      <i className="tiny material-icons">battery_charging_full</i> <b> Energy: </b> {profile.bandwithMax - profile.bandwith} OXY<br />
                      <i className="tiny material-icons">note</i> <b> Sequence: </b> {profile.sequence}<br />
                  </p>
                  <p className="biowrap">
                  </p>
              </div>
              {/* Info */}

              {/* Middle */}
                {
                  (this.state.numNavTag === 1) ? <ProfilePostWall /> :
                  (this.state.numNavTag === 2) ? <ListFollowers idGetListFollow={this.props.profile.user_id}/> :
                  (this.state.numNavTag === 3) ? <ListFollowing idGetListFollow={this.props.profile.user_id}/> :
                  ''
                }
              {/* Middle */}

              <RightSidebar />
            </div>
          </div>

        </div>
    );
  }
}

export default connect(mapStateToProps)(Profile);
