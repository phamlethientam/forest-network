import React, { Component } from 'react';
import { timeStamp2Date } from './../services/utils.service';
import ApiService from './../services/api.service';
import { connect } from 'react-redux'
import Header from '../components/Header';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import Pagination from '../components/Pagination'

const mapStateToProps = (state) => {
  return{
    profile: state.profileReducer.info
  }
}

class TransactionHistory extends Component {
    constructor(props) {
        super(props)
        this.apiService = ApiService()
        this.state = {
            data : [],
            pages: 0,
            page: 1,
            page_numbers: 0,
            isFirst: false,
            isLast: false
        }
    }

    loadData() {
        var isFirstPage = false
        var isLastPage = false
        this.apiService.getTransactionsOfUser(this.state.page, 10).then((res) => {
            var numbers = [];
            if (res.total_page > 2) {
                if (this.state.page === 1)
                    for (var i = 1; i <= 3; i++)
                        numbers.push({
                            value: i,
                            isCurPage: i === this.state.page,
                        });
                else if (this.state.page === res.total_page)
                    for (var j = res.total_page - 2; j <= res.total_page; j++)
                        numbers.push({
                            value: j,
                            isCurPage: j === this.state.page
                        });
                else {
                    numbers.push({
                        value: this.state.page - 1,
                        isCurPage: false
                    });
                    numbers.push({
                        value: this.state.page,
                        isCurPage: true
                    });
                    numbers.push({
                        value: this.state.page + 1,
                        isCurPage: false
                    });
                }
            } else if (res.total_page < 2) {
                isFirstPage = true;
                isLastPage = true;
                numbers.push({
                    value: this.state.page,
                    isCurPage: true
                });
            } else {
                if (this.state.page === 1) {
                    isFirstPage = true;
                    isLastPage = false;
                    numbers.push({
                        value: this.state.page,
                        isCurPage: true
                    });
                    numbers.push({
                        value: this.state.page + 1,
                        isCurPage: false
                    });
                } else {
                    isFirstPage = false;
                    isLastPage = true;
                    numbers.push({
                        value: this.state.page - 1,
                        isCurPage: false
                    });
                    numbers.push({
                        value: this.state.page,
                        isCurPage: true
                    });
                }
            }

            for (var z = 0; z < numbers.length; z++) {
                if (numbers[z].isCurPage === true && z === 0) {
                    isFirstPage = true;
                    break;
                }
                else if (numbers[z].isCurPage === true && z === (numbers.length - 1)) {
                    isLastPage = true;
                }
            }

            this.setState({
                data: res.transactions,
                pages: res.total_page,
                page_numbers: numbers,
                isFirst: isFirstPage,
                isLast: isLastPage
            })
        })
    }

    handleChangePage(index){
        this.setState({
            page: index
        }, () => {
            this.loadData()
        })
    }

    componentDidMount(){
        this.loadData()
    }

    render() {
        const { data, pages, page_numbers} = this.state;
        return (
            <div>
                <Header/>
                {/* Center */}
                <div className="col m12">
                    <ReactTable
                        PaginationComponent={Pagination}
                        data={data}
                        columns={[
                            {
                                Header: "User",
                                columns: [
                                    {
                                        Header: "Public Key",
                                        accessor: "public_key"
                                    }
                                ]
                            },
                            {
                                Header: "Transaction History",
                                columns: [
                                    {
                                        Header: "Received Public Key",
                                        accessor: "public_key_received"
                                    },
                                    {
                                        Header: "Operation",
                                        accessor: "operation",
                                    },
                                    {
                                        Header: "Amount",
                                        accessor: "amount",
                                    },
                                    {
                                        Header: "Memo",
                                        accessor: "memo",
                                    },
                                    {
                                        Header: "Created At",
                                        id: "created_at",   
                                        accessor: d => timeStamp2Date(d.created_at),
                                    }
                                ]
                            }
                        ]}
                        showPageSizeOptions={false}
                        defaultPageSize={10}
                        style={{
                            height: "500px"
                        }}
                        className="-striped -highlight"
                    />
                </div>
                {/* Center */}
                {data.length > 0 &&
                    <ul className="pagination" style={{marginLeft:'450px'}}>
                        {
                            page_numbers.map((item, key) => {
                                if (item.isCurPage) {
                                    if (item.value === 1){
                                        return <li style={{width:'100px'}} key={key} className="waves-effect" onClick={this.handleChangePage.bind(this, 1)}><i className="material-icons">chevron_left</i></li>
                                    }
                                    else
                                    {
                                        return <li style={{width:'100px'}} key={key} className="waves-effect" onClick={this.handleChangePage.bind(this, item.value-1)}><i className="material-icons">chevron_left</i></li>
                                    }
                                }

                                return null
                            })
                        }
                        {!!page_numbers.length && page_numbers.map((item, key) => {
                            if (item.isCurPage) {
                                return <li style={{width:'100px'}} key={key} className="active" onClick={this.handleChangePage.bind(this, item.value)}>{item.value}</li>
                            }

                                return <li style={{width:'100px'}} key={key} className="waves-effect" onClick={this.handleChangePage.bind(this, item.value)}>{item.value}</li>
                        })
                        }
                        {
                            page_numbers.map((item, key) => {
                                if (item.isCurPage) {
                                    if (item.value === pages){
                                        return <li style={{width:'100px'}} key={key} className="waves-effect" onClick={this.handleChangePage.bind(this, pages)}><i className="material-icons">chevron_right</i></li>
                                    }
                                    else
                                    {
                                        return <li style={{width:'100px'}} key={key} className="waves-effect" onClick={this.handleChangePage.bind(this, item.value+1)}><i className="material-icons">chevron_right</i></li>
                                    }
                                }

                                return null
                            })
                        }
                    </ul>
                }
            </div>
        );
    }
}

export default connect(mapStateToProps)(TransactionHistory);
