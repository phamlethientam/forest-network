import React, { Component } from 'react';
import { connect } from 'react-redux';
import ApiService from './../services/api.service';
import Header from '../components/Header';
import PostsWall from '../components/PostsWall';
import RightSidebar from '../components/RightSidebar';
import { Link } from 'react-router-dom';
import defaultAvatar from './../img/default-avatar.png'

const mapStateToProps = (state) => {
  return{
    profile: state.profileReducer.info
  }
}

class Home extends Component {
  constructor(props){
    super(props)
    this.apiService = ApiService()
  }


  render() {
    return (
        <div>
          <Header/>
          <div className="bgcontainers">
            <div className="row">
              {/* Left */}
              <div className="col m3">
                <div className="card">
                  <div className="card-image">
                    <img alt="" src="images/headerprofil.jpg" />
                  </div>
                  <div className="card-content">
                    <span className="card-title">
                      <Link to="/profile"><img src={this.props.profile.avatar ? this.props.profile.avatar : defaultAvatar} alt="" className="profilecircle"/></Link>
                      <div style={{ marginTop: "-20px" }}>
                        <Link to="/profile">
                          <b>{this.props.profile.username}</b>
                        </Link>
                      </div>
                    </span>
                    <div className="row">
                      <div className="col s4">
                        <small>Following</small>
                        <br />
                        <b>{this.props.profile.following.length}</b>
                      </div>
                      <div className="col s4">
                        <small>Followers</small>
                        <br />
                        <b>{this.props.profile.follower.length}</b>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* Left */}

              {/* Center */}
              <PostsWall/>
              {/* Center */}

              {/* Right */}
              <RightSidebar/>
              {/* Right */}
            </div>
          </div>
        </div>  
    );
  }
}

export default connect(mapStateToProps)(Home);
