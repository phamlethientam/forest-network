import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ApiService from './../services/api.service';
import CryptoJS from 'crypto-js';
import { keyStorage } from './../constants/localStorage';
import { loadItem } from './../services/storage.service';
import { SecretKey } from './../constants/crypto';
import base32 from 'base32.js';
import transaction from './../lib/transaction';
import { calcBandwithConsume } from './../services/utils.service';
import Header from '../components/Header';
import UserCard from '../components/UserCard';
import {Input , Button} from 'react-materialize';

const mapStateToProps = (state) => {
  return{
    profile: state.profileReducer.info
  }
}

class ListUser extends Component {
  static propTypes = {
    profile: PropTypes.object,
    saveProfileFromApi: PropTypes.func
  }

  constructor(props){
    super(props)
    this.apiService = ApiService()
    this.state = {
      users: [],
      page: 1,
      total_page: 0,
      showConfirm: false,
      inputSearch: '',
      ShowLoadMore: 'block',
      spublic_key: '',
      susername: ''
    }
  }

  handleChangeInputSearchPublickey(e){
    this.setState({
      spublic_key: e.target.value
    })
  }

  handleChangeInputSearchUsername(e){
    this.setState({
      susername: e.target.value
    })
  }

  SearchPublicKey(){
    this.setState({
      ShowLoadMore: 'none',
      users: []
    })

		this.apiService.getSearchUsers('PublicKey',this.state.spublic_key ).then((res) => {
			res.users.map(user => {
				return(this.setState ({
					users: this.state.users.concat({
						user_id: user.user_id,
						public_key: user.public_key,
						avatar:user.avatar,
						username: user.username
          })
				}))
			})		
		})
  }

  SearchUserName(){
    this.setState({
      ShowLoadMore: 'none',
      users: []
    })

		this.apiService.getSearchUsers('Username', this.state.susername).then((res) => {
			res.users.map(user => {
				return(this.setState ({
					users: this.state.users.concat({
						user_id: user.user_id,
						public_key: user.public_key,
						avatar:user.avatar,
						username: user.username
          })
				}))
			})		
    })
  }

  handleShowConfirm(){
    if(!this.state.showConfirm){
      this.setState({
        showConfirm: true
      })
    }
  }

  handleLoadMore(){
    this.apiService.getUnfollowedUsers(this.props.profile.user_id, this.state.page, 20).then((data) => {
      this.setState({
        users: [...this.state.users, ...data.users],
        total_page: data.total_page,
        page: this.state.page + 1
      })
    })
  }

  handleSubmit(){
    let temp = loadItem(keyStorage.private_key)
    let my_private_key = CryptoJS.AES.decrypt(temp, SecretKey).toString(CryptoJS.enc.Utf8)

    this.apiService.getPubkeysFollowing(this.props.profile.public_key, this.props.profile.following).then((data) => {
      let listPubkey = {
        addresses: []
      }
      
      data.forEach((item) => {
        listPubkey.addresses.push(Buffer.from(base32.decode(item.public_key)))
      })
      this.apiService.getCurrentProfile().then((userProfile) => {
        const profile = userProfile
        const tx = {
          version: 1,
          sequence: profile.sequence + 1,
          memo: Buffer.alloc(0),
          account: profile.public_key,
          operation: "update_account",
          params: {
            key: 'followings',
            value: listPubkey
          },
          signature: new Buffer(64)
        }

        let consume = calcBandwithConsume(profile, transaction.encode(tx).toString('base64'), new Date())
        if(consume > profile.bandwithMax){
          alert('Not enough OXY')
          return
        }

        transaction.sign(tx, my_private_key);
        let TxEncode = '0x' + transaction.encode(tx).toString('hex');
        this.apiService.updateListFollow(TxEncode).then((flag) => {
          if(flag === 'success'){
            this.setState({
              showConfirm: false,
            })
            alert('Success !')
          }
          else{
            alert('Fail !')
          }
        })
      })
    })
  }

  componentDidMount(){
    this.handleLoadMore()
  }

  render() {
    let {users} = this.state
    return (
        <div>
          <Header/>
              <div className="container">
                <ul className="left hide-on-med-and-down" style={{width:'550px'}}>
                  <Input s={6} label="Public key" onChange={this.handleChangeInputSearchPublickey.bind(this)}/>
                  <Button waves='light' onClick={this.SearchPublicKey.bind(this)}>Search Public key</Button>      
                </ul>
                <ul className="right hide-on-med-and-down" style={{width:'350px'}}>
                  <Input s={6} label="User name" onChange={this.handleChangeInputSearchUsername.bind(this)}/>
                  <Button waves='light' onClick={this.SearchUserName.bind(this)}>Search Username</Button>     
                </ul>     
              </div>
          <div style={{ clear: "both" }} />

          <div className="bgcontainers">
            <div className="row">

              {/* Center */}
              <div className="col m12">
                    {!!users.length && users.map((item, key) => {
                        return(
                            <div className="col m3" key={key}>
                                <UserCard user={item} onShowConfirm={this.handleShowConfirm.bind(this)}/>
                            </div>
                            )
                        })
                    }
              </div>
              
              {/* Center */}
                {
                  this.state.page <= this.state.total_page &&
                  <div style={{textAlign:'center',display:this.state.ShowLoadMore}}>
                    <Button onClick={this.handleLoadMore.bind(this)} className='blue'> LOAD MORE </Button>
                  </div>
                }

                {
                  this.state.showConfirm ?
                  /*eslint-disable-next-line*/
                  <a onClick={this.handleSubmit.bind(this)}> 
                    <Button floating fab='horizontal' tooltip="confirm" icon='check' className='red' large style={{bottom: '45px', right: '24px'}}>
                    </Button> 
                  </a>             
                  : 
                  null
                }
            </div>
          </div>
        </div>  
    );
  }
}

export default connect(mapStateToProps)(ListUser);
