import React, { Component } from 'react';
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import ApiService from './../services/api.service'
import { withRouter } from 'react-router'
import Header from '../components/Header';
import OtherPost from '../components/OtherPost';
import ListFollowers from '../components/ListFollowers';
import ListFollowing from '../components/ListFollowing';
import RightSidebar from '../components/RightSidebar';
import defaultAvatar from './../img/default-avatar.png';
import {Button} from 'react-materialize';
import { loadItem } from './../services/storage.service';
import { keyStorage } from './../constants/localStorage';
import { SecretKey } from './../constants/crypto';
import { calcBandwithConsume } from './../services/utils.service';
import CryptoJS from 'crypto-js';
import base32 from 'base32.js';
import transaction from './../lib/transaction';

const mapStateToProps = (state) => {
    return {
        profile: state.profileReducer.info
    }
}

class Others extends Component {
    static propTypes = {
        profile: PropTypes.object,
        idGetListFollow: PropTypes.number
    }
    constructor(props) {
        super(props)
        this.apiService = ApiService()
        this.state = {
            numNavTag: 1,
            intervalId: 0,
            balance: 0,
            currentEnergy: 0,
            consumedEnergy: 0,
            avatar: "",
            username: "",
            followers: 0,
            following: 0,
            sequence: 0,
            isFollow: false,
        }
    }

    handleTimeline() {
        this.setState({
            numNavTag: 1
        })
    }

    handleFollowers() {
        this.setState({
            numNavTag: 2
        })
    }

    handleFollowing() {
        this.setState({
            numNavTag: 3
        })
    }

    UNSAFE_componentWillReceiveProps(props){
      if(this.props.match.params.id !== props.match.params.id){
        this.setState({
          numNavTag: 1
        })
        this.loadInfoUser(props)
      }
    }

    loadInfoUser(props){
      this.apiService.getInfoUser(props.match.params.id).then((res) => {
            res.info_user.follower.forEach((item)=>{
                if(item === this.props.profile.user_id)
                    this.setState({
                        isFollow: true
                    })
            })
          
          this.setState({
              balance: res.info_user.amount,
              currentEnergy: res.info_user.bandwithMax - res.info_user.bandwith,
              consumedEnergy: res.info_user.bandwith,
              avatar: res.info_user.avatar,
              username: res.info_user.username,
              followers: res.info_user.follower.length,
              following: res.info_user.following.length,
              sequence: res.info_user.sequence,
              public_key: res.info_user.public_key,
          })
      })
    }
    componentDidMount() {
        if(+this.props.match.params.id === +this.props.profile.user_id){
          this.props.history.push('/profile')
        }
        this.loadInfoUser(this.props)
    }

    clickFollow(){
        let temp = loadItem(keyStorage.private_key)
        let my_private_key = CryptoJS.AES.decrypt(temp, SecretKey).toString(CryptoJS.enc.Utf8)
    
        this.apiService.getPubkeysFollowing(this.props.profile.public_key, this.props.profile.following).then((data) => {
            let listPubkey = {
                addresses: []
            }

            if(this.state.isFollow === true){
                data.forEach((item) => {
                    if(item.public_key !== this.state.public_key)
                        listPubkey.addresses.push(Buffer.from(base32.decode(item.public_key)))
                })
            }
            else{
                data.forEach((item) => {
                    listPubkey.addresses.push(Buffer.from(base32.decode(item.public_key)))
                })
                listPubkey.addresses.push(Buffer.from(base32.decode(this.state.public_key)))
            }

          this.apiService.getCurrentProfile().then((userProfile) => {
            const profile = userProfile
            const tx = {
              version: 1,
              sequence: profile.sequence + 1,
              memo: Buffer.alloc(0),
              account: profile.public_key,
              operation: "update_account",
              params: {
                key: 'followings',
                value: listPubkey
              },
              signature: new Buffer(64)
            }
    
            let consume = calcBandwithConsume(profile, transaction.encode(tx).toString('base64'), new Date())
            if(consume > profile.bandwithMax){
              alert('Not enough OXY')
              return
            }
    
            transaction.sign(tx, my_private_key);
            let TxEncode = '0x' + transaction.encode(tx).toString('hex');
            this.apiService.updateListFollow(TxEncode).then((flag) => {
              if(flag === 'success'){
                this.setState({
                    isFollow: !this.state.isFollow,
                })
                alert('Success !')
              }
              else{
                alert('Fail !')
              }
            })
          })
        })
    }

    render() {
        return (
            <div>
                <Header/>
    
                {/* Cover IMG */}
                <div className="headp">
                    <img src="http://gg.gg/clhfc" alt=""/>
                </div>
                {/* Cover IMG */}
    
                {/* Menu */}
                <nav className="bottomnav">
                    <div>
                    {/*eslint-disable-next-line*/}
                    <a data-target="mobile-demo" className="sidenav-trigger">
                        <i className="material-icons">menu</i>
                    </a>
                    <ul className="bottomul left hide-on-med-and-down">
                        <li>
                            <img src={this.state.avatar ? this.state.avatar : defaultAvatar} width="150" height="150" alt="" className="circle photoprofile" ></img>
                        </li>
                        <li onClick={this.handleTimeline.bind(this)}>
                        {/*eslint-disable-next-line*/}
                        <a>
                            <small>
                            Posts <span>Home page</span>
                            </small>
                        </a>
                        </li>
                        <li onClick={this.handleFollowers.bind(this)}>
                        {/*eslint-disable-next-line*/}
                        <a >
                            <small>
                            Followers  <span>{this.state.followers}</span>
                            </small>
                        </a>
                        </li>
                        <li onClick={this.handleFollowing.bind(this)}>
                        {/*eslint-disable-next-line*/}
                        <a >
                            <small>
                            Following <span>{this.state.following}</span>
                            </small>
                        </a>
                        </li>
                    </ul>
                    <ul className="right hide-on-med-and-down">
                        {
                            this.state.isFollow ? 
                            <Button waves='light' className='red' onClick={this.clickFollow.bind(this)}>UnFollow</Button> : 
                            <Button waves='light' className='blue' onClick={this.clickFollow.bind(this)}> Follow</Button>
                        }
                    </ul>
                    </div>
                </nav>
                <div style={{ clear: "both" }} />
                {/* Menu */}
    
                <div className="bgcontainers">
                    <div className="row">
                        {/* Info */}
                        <div className="col m3">
                            {/* profil */}
                            <p className="biowrap" />
                                <h5 className="profidentity">
                                    {this.state.username}
                                </h5>
                            <p/>
                            <p className="biowrap" style={{wordWrap:'break-word'}}>{this.state.public_key} </p>
                            <p className="biowrap">
                                <i className="tiny material-icons">attach_money</i> <b> Balance: </b> {this.state.balance} CEL<br />
                                <i className="tiny material-icons">battery_charging_full</i> <b> Energy: </b> {this.state.currentEnergy} OXY<br />
                                <i className="tiny material-icons">note</i> <b> Sequence: </b> {this.state.sequence} <br />
                            </p>
                            <p className="biowrap">
                            </p>
                        </div>
                        {/* Info */}

                        {/* Middle */}
                            {
                            (this.state.numNavTag === 1) ? <OtherPost user_id={this.props.match.params.id} />:
                            (this.state.numNavTag === 2) ? <ListFollowers idGetListFollow={+this.props.match.params.id}/> :
                            (this.state.numNavTag === 3) ? <ListFollowing idGetListFollow={+this.props.match.params.id}/> :
                            ''
                            }
                        {/* Middle */}

                        <RightSidebar />        

                    </div>
                </div>
            </div>
        );
    }
};

export default withRouter(connect(mapStateToProps)(Others));
