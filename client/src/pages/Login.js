import React, { Component } from 'react';
import ApiService from './../services/api.service';
import { Keypair } from 'stellar-base';
import { withRouter } from 'react-router';
import { saveProfileFromApi } from './../actions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import CryptoJS from 'crypto-js';
import { keyStorage } from './../constants/localStorage';
import { saveItem } from './../services/storage.service';
import { SecretKey } from './../constants/crypto';

const mapDispatchToProps = (dispatch) => {
  return{
    saveProfileFromApi: (info) => {dispatch(saveProfileFromApi(info))},
  }
}

class SignInUp extends Component {
  static propTypes = {
    saveProfileFromApi: PropTypes.func
  }

  constructor(props){
    super(props)
    this.apiService = ApiService()
    this.state = {
      private_key: '',
      block: true,
      errorLogin: '',
    }
  }

  componentDidMount(){
    this.apiService.getCurrentProfile().then((data) => {
      if(data){
        this.props.history.push('/')
      }
      else{
        this.setState({
          block: false
        })
      }
    })
  }

  handleLogin(e){
    e.preventDefault();

    if(!this.state.private_key){
      this.setState({
        errorLogin: 'Private key is empty!'
      })
      return
    }

    let key = null
    try{
      key = Keypair.fromSecret(this.state.private_key)
    }
    catch(e){
      this.setState({
        errorLogin: "Invalid private key!"
      })
      return
    }
    let public_key = key.publicKey()

    this.apiService.login(public_key).then((data) => {
      if(data){
        let key = CryptoJS.AES.encrypt(this.state.private_key, SecretKey)
        saveItem(keyStorage.private_key, key.toString())
        // let b = CryptoJS.AES.decrypt(key.toString(), "SecretKey").toString(CryptoJS.enc.Utf8)
        this.props.saveProfileFromApi && this.props.saveProfileFromApi(data)
        this.props.history.push('/')
      }
      else{
        this.setState({
          errorLogin: "Private key is not registered!"
        })
      }
    })
  }

  handleChangePrivateKey(e){
    this.setState({
      private_key: e.target.value
    })
  }

  render() {
    if(this.state.block){
      return null
    }
    return (
        <div className="container">
            <div className="row">
                <div className="col s12 m6 offset-m3">
                <div className="card">
                    <div className="card-content">
                    <span className="card-title">Log in to Forest Network. </span>   
                    <div className="row">
                        <div className="col s12">
                            <div className="row">
                                <div className="input-field col s12">
                                    <i className="material-icons prefix">
                                      enhanced_encryption
                                    </i>
                                    <input id="icon_prefix" type="text" value={this.state.private_key} onChange={this.handleChangePrivateKey.bind(this)} className="validate"/>
                                    <label htmlFor="icon_prefix">Private key</label>
                                </div>
                                <div className="input-field col s12">
                                <button onClick={this.handleLogin.bind(this)} className="waves-effect waves-light btn-small">
                                    Log In
                                </button>
                                </div>
                                <span style={{marginLeft:'100px',color:'red',fontFamily : "'Playfair Display', serif"}}>
                                    {this.state.errorLogin}
                                </span>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div className="card-action">
                    © 2018 Forest Network
                    </div>
                </div>
                </div>
            </div>
        </div>      
    );
  }
}

export default withRouter(connect(null, mapDispatchToProps)(SignInUp));
