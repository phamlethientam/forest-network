import React, { Component } from 'react';

class NotFoundPage extends Component {
    render() {
        return (
            <div className="pnf" style={{fontFamily : "'Cabin Sketch', serif"}}>
                <h1 style={{fontSize:"5rem"}}>woop</h1>
                <h2 style={{fontSize:"4rem"}}>page not found</h2>
                <h2 style={{fontSize:"4rem"}}>we are working on it</h2>
            </div>
        );
    }
}

export default NotFoundPage;