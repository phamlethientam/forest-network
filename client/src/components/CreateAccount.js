import React, { Component } from 'react'
import ApiService from './../services/api.service'
import transaction from './../lib/transaction';
import { calcBandwithConsume } from './../services/utils.service';
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { keyStorage } from './../constants/localStorage';
import { loadItem } from './../services/storage.service';
import { SecretKey } from './../constants/crypto';
import CryptoJS from 'crypto-js';
import {Modal,Button} from 'react-materialize'
import { Keypair } from 'stellar-base';

const mapStateToProps = (state) => {
  return{
    profile: state.profileReducer.info
  }
}

class CreateAccount extends Component {
  static propTypes = {
    profile: PropTypes.object,
  }

  constructor(props){
    super(props)
    this.apiService = ApiService()
    this.state = {
      your_public_key: '',
      isShow: 'none',
      dataRegister: null
    }
  }


  handleSubmit(){
    if(!this.state.your_public_key){
        alert('Empty public key !')
        return
    }

    this.apiService.getCurrentProfile().then((data) => {
      const profile = data
      const tx = {
        version: 1,
        sequence: profile.sequence + 1,
        memo: Buffer.alloc(0),
        account: profile.public_key,
        operation: "create_account",
        params: {
          address: this.state.your_public_key,
        },
        signature: new Buffer(64)
      }

      if(this.state.your_public_key === profile.public_key){
        alert('Invalid publickey!')
        return
      }

      try{
        transaction.encode(tx).toString('base64')
      }
      catch(e){
        alert('Invalid publickey!')
        return
      }

      let consume = calcBandwithConsume(profile, transaction.encode(tx).toString('base64'), new Date())
      if(consume > this.props.profile.bandwithMax){
        alert('Not enough energy !')
        return
      }

      let temp = loadItem(keyStorage.private_key)
      let my_private_key = CryptoJS.AES.decrypt(temp, SecretKey).toString(CryptoJS.enc.Utf8)

      transaction.sign(tx, my_private_key);
      let TxEncode = '0x' + transaction.encode(tx).toString('hex');

      this.apiService.createAccount(TxEncode).then((data) => {
        if(data === 'success'){
            alert('Create success!')
            this.setState({
                your_public_key: ''
            })
        }
        else{
          alert('Fail! Input another publickey')
        }
      })
    })
  }

  handleChangePublicKey(e){
    this.setState({
      your_public_key: e.target.value
    })
  }

  handleGenerate(){
    let key = Keypair.random()
    let data = {
      secret_key: key.secret(),
      public_key: key.publicKey()
    }
    this.setState({
      dataRegister: data
    }, () => {
      this.setState({
        isShow : 'block'
      })
    })
  }

  render() {
    return (
        <Modal fixedFooter header='Create Account'
                trigger={
                    /*eslint-disable-next-line*/
                    <a className="waves-effect waves-light btn-small">
                        <span style={{fontFamily : "'Playfair Display', serif",color: "white"} }>Create Account</span>
                    </a>
                }
                actions={
                    <Button waves='light' onClick={this.handleSubmit.bind(this)}>Create</Button>
                } 
        >
            <div className="row">
                <div className="input-field col s12">
                    <i className="material-icons prefix">
                        enhanced_encryption
                    </i>
                    <input id="icon_prefix" value={this.state.your_public_key} onChange={this.handleChangePublicKey.bind(this)} type="text" className="validate" />
                    <label htmlFor="icon_prefix">New Public Key</label>
                </div>
                <div className="input-field col s12">
                    <Button waves='light' onClick={this.handleGenerate.bind(this)}>Generate Keypair</Button>
                    <div style={{display: this.state.isShow}}>
                        <br/>
                        <span style={{fontWeight: 'bold'}}>Private Key:</span> {this.state.dataRegister ? this.state.dataRegister.secret_key : ''}
                        <br/>
                        <span style={{fontWeight: 'bold'}}>Public Key: </span>{this.state.dataRegister ? this.state.dataRegister.public_key : ''}
                    </div>
                </div>
            </div>           
        </Modal> 
    );
  }
}

export default connect(mapStateToProps)(CreateAccount);
