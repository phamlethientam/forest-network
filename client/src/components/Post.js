import React, { Component,Fragment } from 'react';
import PropTypes from 'prop-types';
import {fromNowDate,calcBandwithConsume} from './../services/utils.service';
import { connect } from 'react-redux'
// import {Button} from 'react-materialize'
// import sad from '../img/sad.png'
// import haha from '../img/haha.png'
// import angry from '../img/angry.png'
// import wow from '../img/wow.png'
import defaultAvatar from './../img/default-avatar.png'
import { Link } from 'react-router-dom'
import { saveProfileFromApi } from './../actions'
import ApiService from './../services/api.service'
import { SecretKey } from './../constants/crypto'
import CryptoJS from 'crypto-js'
import { keyStorage } from './../constants/localStorage'
import { loadItem } from './../services/storage.service'
import transaction from './../lib/transaction'


const mapDispatchToProps = (dispatch) => {
    return{
        saveProfileFromApi: (info) => {dispatch(saveProfileFromApi(info))}
    }
}
  
const mapStateToProps = (state) => {
    return{
        profile: state.profileReducer.info,
    }
}

class Post extends Component {
    static propTypes = {
        post: PropTypes.object,
        profile: PropTypes.object,
        saveProfileFromApi: PropTypes.func
      }
    
      constructor(props){
        super(props)
        this.apiService = ApiService()
        this.state = {
          post: this.props.post,
          content: '',
          comments: [],
          number_of_comments: 0,
          number_of_reacts: 0,
          page: 1,
          total_page: 0,
          react: 0,
          isShowError: false,
          error: '',
          isShowSuccess: false
        }
      }
    
      componentDidMount(){
        this.loadComments()
        this.apiService.getPostReacts(this.props.post.id).then((data) => {
          if(data){
            this.setState({
              number_of_reacts: data.total_reacts,
              react: data.your_react
            })
          }
        })
      }
    
      loadComments(){
        this.apiService.getPostComments(this.props.post.id, this.state.page, 2).then((data) => {
          if(data){
            this.setState({
              comments: [...this.state.comments, ...data.comments],
              number_of_comments: data.total_item,
              page: this.state.page + 1,
                        total_page: data.total_page
            })
          }
        })
      }
    
      handleContentChange(e) {
        this.setState({
          content: e.target.value
        });
      }
    
      handleOnSubmit(){
        if(this.state.content){
          this.apiService.getCurrentProfile().then((data) => {
            this.props.saveProfileFromApi && this.props.saveProfileFromApi(data)
            const plainTextContent = {
              type: 1,
              text: this.state.content
            }
            let tx = {
              version: 1,
              account: data.public_key,
              sequence: data.sequence + 1,
              memo: Buffer.alloc(0),
              operation: "interact",
              params: {
                object: this.props.post.hash,
                content: plainTextContent
              },
              signature: new Buffer(64)
            }
            let temp = loadItem(keyStorage.private_key)
            let my_private_key = CryptoJS.AES.decrypt(temp, SecretKey).toString(CryptoJS.enc.Utf8)
            transaction.sign(tx, my_private_key);
            let TxEncode = '0x' + transaction.encode(tx).toString('hex');
    
            const consume = calcBandwithConsume(data, transaction.encode(tx).toString('base64'), new Date());
            if (consume > data.bandwithMax) {
              alert('Not enough OXY')
              return;
            } else {
              this.apiService.postComment(TxEncode).then((status) => {
                if (status === 'success') {
                  const tempComment = {
                    User: {
                      avatar: this.props.profile.avatar,
                      user_id: this.props.profile.user_id,
                      username: this.props.profile.username
                    },
                    content: this.state.content,
                    created_at: new Date(),
                    post_id: this.props.post.id,
                    user_id: this.props.profile.user_id
                  }
                  this.setState({
                    content: '',
                    comments: [
                      tempComment, ...this.state.comments
                    ],
                    number_of_comments: this.state.number_of_comments + 1
                  })
                } else {
                  alert('Fail to comment')
                }
              })
            }
            })
        }
      }
    
      handleKeyPress(event){
        if(event.key === 'Enter' && !event.altKey){
          event.preventDefault();
          event.target.value = ""
          this.handleOnSubmit()
        } else if (event.key === 'Enter' && !event.altKey){
          event.preventDefault();
          event.target.value = ""
        }
        else if(event.key === 'Enter' && event.altKey){
          let textarea = event.target
          let pos = textarea.selectionStart
          let first = textarea.value.substring(0, pos) + "\n"
          let rest = textarea.value.substring(pos)
          this.setState({
            content: first + rest
          }, () => {
            textarea.selectionStart = pos + 1
            textarea.selectionEnd = pos + 1
          })
        }
      }
    
      handleLoadMore(){
        this.loadComments()
      }
    
      handleErrorImg(e){
        e.target.onerror = null;
        e.target.src = defaultAvatar
      }
    

  render() {
    let {post} = this.state
    let {profile} = this.props
    return (
        <Fragment>
            <li className="collection-item avatar">
                <Link to={'/user/' + post.user_id}>
                    <img alt="avatar" src={post.avatar ? post.avatar : defaultAvatar} className="circle" />
                </Link>
                <span className="title">
                    <Link to={'/user/' + post.user_id}>
                        <strong>{post.username}</strong> <br/>
                        <small>{fromNowDate(post.created_on)}</small>
                    </Link>
                </span>
                <br />
                <br />
                <p>
                    {   
                      post.content.split('\n').map((itemChild, key) => {
                        return <span key={key}>{itemChild}<br/></span>
                      })
                    }
                </p>
                <br />    
                <div style={{clear: 'both'}}></div>
                <span className="texttweet">
                  {/*eslint-disable-next-line*/}
                  <a >{this.state.number_of_comments} Comments</a>
                </span>
                <span className="texttweetimage">
                  {/*eslint-disable-next-line*/}
                  <a >{this.state.number_of_reacts} Reacts</a>
                </span>
                <br/> 
                <br/>
            </li>
            <li className="collection-item avatar">
                <img src={profile.avatar ? profile.avatar : defaultAvatar} alt="" className="circle"/>
                <span className="title">
                  {/*eslint-disable-next-line*/}
                  <a>
                    <strong>{profile.username}</strong>
                  </a>
                </span>
                <div className="row">
                    <div className="input-field col s12">
                        <textarea className="materialize-textarea areatweet" 
                          value={this.state.content} 
                          onKeyPress={this.handleKeyPress.bind(this)} 
                          onChange={this.handleContentChange.bind(this)}>
                        </textarea>
                    </div>
                </div>
            </li>
            {!!this.state.comments.length && this.state.comments.map((item, key) => {
              return(
                  <li className="collection-item avatar" key={key}>
                    <Link to={"/user/" + item.User.user_id}>
                      <img src={item.User.avatar ? item.User.avatar : defaultAvatar} alt="" className="circle" onError={this.handleErrorImg.bind(this)}/>
                    </Link>
                    <span className="title">
                      <Link to={"/user/" + item.User.user_id}>
                        <strong>{item.User.username}</strong> <br/>
                        <small>{fromNowDate(item.created_at)}</small>
                      </Link></span><br/><br/>
                      <p>
                      {
                        item.content.split('\n').map((itemChild, key) => {
                          return <span key={key}>{itemChild}<br/></span>
                        })
                      }
                      </p>
                      <br/>
                  </li>
                )
              })
            }
            {
              this.state.page <= this.state.total_page &&
              <li className="collection-item avatar">
                {/*eslint-disable-next-line*/}
                <a className="cc" onClick={this.handleLoadMore.bind(this)}>Show more comments</a>
              </li>
            }
            <br/>
            <br/>
        </Fragment>    
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Post);
