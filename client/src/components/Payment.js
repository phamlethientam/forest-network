import React, { Component } from 'react';
import transaction from './../lib/transaction';
import { connect } from 'react-redux'
import ApiService from './../services/api.service'
import { calcBandwithConsume } from './../services/utils.service';
import PropTypes from 'prop-types'
import { keyStorage } from './../constants/localStorage';
import { loadItem } from './../services/storage.service';
import { SecretKey } from './../constants/crypto';
import CryptoJS from 'crypto-js';
import {Modal,Button} from 'react-materialize'

const mapStateToProps = (state) => {
  return{
    profile: state.profileReducer.info
  }
}

class Payment extends Component {
  static propTypes = {
    profile: PropTypes.object,
    saveProfileFromApi: PropTypes.func
  }

  constructor(props){
    super(props)
    this.apiService = ApiService()
    this.state = {
      your_public_key: '',
      amount: ''
    }
  }

  handleChangeAmount(e){
    this.setState({
      amount:  +e.target.value
    })
  }

  handleChangeYourPublicKey(e){
    this.setState({
      your_public_key: e.target.value
    })
  }



  handleSubmit(){

    if(this.state.amount <= 0){
        alert('Your balance = 0 !') 
        return
    }
    if(this.state.your_public_key === ''){
        alert('Transfer fail !') 
        return 
    }

    this.apiService.getCurrentProfile().then((userProfile) => {
      const profile = userProfile
      const tx = {
        version: 1,
        sequence: profile.sequence + 1,
        memo: this.state.memo ? Buffer.from(this.state.memo) : Buffer.alloc(0),
        account: profile.public_key,
        operation: "payment",
        params: {
          address: this.state.your_public_key,
          amount: this.state.amount
        },
        signature: new Buffer(64)
      }

      let temp = loadItem(keyStorage.private_key)
      let my_private_key = CryptoJS.AES.decrypt(temp, SecretKey).toString(CryptoJS.enc.Utf8)

      if(this.state.your_public_key === profile.public_key){
        alert('Invalid publickey!')
        return
      }

      try{
        transaction.encode(tx).toString('base64')
      }
      catch(e){
        alert('Invalid publickey!')
        return
      }

      let consume = calcBandwithConsume(profile, transaction.encode(tx).toString('base64'), new Date())
      if(consume > profile.bandwithMax){
        alert('Not enough energy !')
        return
      }

      transaction.sign(tx, my_private_key);
      let TxEncode = '0x' + transaction.encode(tx).toString('hex');
      this.apiService.conductTransaction(TxEncode).then((status) => {
        if(status === 'success'){
            alert('Transfer success!')
            this.setState({
                your_public_key: '',
                amount: 0
            })
        }
        else{
            alert('Transfer fail!')
        }
      })
    })
  }


  render() {
    return (
        <Modal fixedFooter header='Payment'
            trigger={
              /*eslint-disable-next-line*/
                <a className="waves-effect waves-light btn-small">
                    <span style={{fontFamily : "'Playfair Display', serif",color: "white"} }>Payment</span>
                </a>
            }
            actions={
                <Button waves='light' onClick={this.handleSubmit.bind(this)}>Transfer</Button>
            } 
        >
            <div className="row">
                <div className="input-field col s12">
                    <i className="material-icons prefix">
                        enhanced_encryption
                    </i>
                    <input id="icon_prefix"  type="text" value={this.state.your_public_key} onChange={this.handleChangeYourPublicKey.bind(this)} className="validate" />
                    <label htmlFor="icon_prefix">Receiver publickey</label>
                </div>
                <div className="input-field col s12">
                    <i className="material-icons prefix">
                        attach_money
                    </i>
                    <input id="icon_prefix" value={this.state.amount} onChange={this.handleChangeAmount.bind(this)}  type="number" className="validate" />
                    <label htmlFor="icon_prefix">Amount of CEL</label>
                </div>
            </div>  
        </Modal> 
    );
  }
}

export default connect(mapStateToProps)(Payment);
