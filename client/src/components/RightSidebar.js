import React, { Component } from 'react';
import ApiService from './../services/api.service';
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import RightSidebarItem from './RightSidebarItem';
import { Link } from 'react-router-dom';
const mapStateToProps = (state) => {
    return{
        profile: state.profileReducer.info
    }
}

class RightSidebar extends Component {
    static propTypes = {
        profile: PropTypes.object,
        saveProfileFromApi: PropTypes.func
    }
    
    constructor(props){
        super(props)
        this.apiService = ApiService()
        this.state = {
            users: []
        }
    }
    
    componentDidMount(){
        const {profile} = this.props
        this.apiService.getUnfollowedUsers(profile.user_id, 1, 3).then((data) => {
            if(data){
                this.setState({
                users: data.users
                })
            }
        })
    }

    render() {
        return (
            <div className="col m3">
                <div className="card">
                    <div className="card-content">
                        <span className="card-title">
                            Who to follow 
                            <br/>
                            <small>
                                <Link to='/listuser'>View All</Link>
                            </small>
                        </span>
                        <ul className="collection" style={{ border: "none" }}>
                            {!!this.state.users.length && this.state.users.map((item, key) => {
                                    return(
                                        <li className="collection-item avatar" key={key}>
                                            <RightSidebarItem user={item}/>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </div>

                </div>

                <div className="card">
                    <div className="card-content">
                    <small className="linka">
                        © 2018 Forest Nework 
                    </small>
                    </div>
                </div>
            </div>                
        );
    }
}

export default connect(mapStateToProps)(RightSidebar);