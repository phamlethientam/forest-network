import React, { Component } from 'react';
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import ApiService from './../services/api.service'
import InfiniteScroll from 'react-infinite-scroller'
import {ProgressBar,Col} from 'react-materialize'
import Post from './Post'

const mapStateToProps = (state) => {
    return {
        profile: state.profileReducer.info
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}

class OthersPost extends Component {
    constructor(props) {
        super(props)
        this.apiService = ApiService()
        this.state = {
            page: 1,
            dataPosts: [],
            hasMoreItems: true
        }
    }

    static propTypes = {
        posts: PropTypes.array
    }

    loadItems(page) {
        this.apiService.getMyPosts(this.props.user_id, this.state.page, 10).then((res) => {
            res.posts.forEach((element) => {
                element.comments = []
                element.likes = 100
                element.authorize = "Shared publicly"
            })
            this.setState ({
              dataPosts: [...this.state.dataPosts, ...res.posts],
              page: this.state.page + 1,
              pages: res.total_page,
            }, () => {
                if (this.state.page > res.total_page) {
                    this.setState({
                        hasMoreItems: false
                    })
                }
            })
          })
    }

    render() {
        const { dataPosts } = this.state
        return (
            <div className="col m6">
                <ul className="collection" style={{ border: "none" }}>
                    {/*Post */}
                    <InfiniteScroll pageStart={0} 
                        loadMore={this.loadItems.bind(this)} 
                        hasMore={this.state.hasMoreItems}
                        threshold={100}
                        loader={  <Col s={12}> <ProgressBar />  </Col>  }>
                        {   dataPosts.length > 0
                                    ?
                            dataPosts.map((item) => {
                                const postTemplate = {
                                    id: item.id,
                                    avatar: item.User.avatar,
                                    user_id: item.user_id,
                                    username: item.User.username,
                                    authorize: "Shared publicly",
                                    created_on: item.created_at,
                                    likes: 100,
                                    isLike: false,
                                    content: item.content,
                                    comments: []
                                }
                                return (
                                    <Post key={item.id} post={postTemplate} />
                                )
                            })
                            :
                            <li className="collection-item avatar">
                                <h2 style={{fontFamily:"'Lobster', cursive",marginLeft:'100px'}}>Sorry No Posts</h2>
                            </li>
                        }
                    </InfiniteScroll>
                    {/*Post */}
                    
                </ul>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OthersPost);
