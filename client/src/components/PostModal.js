import React, { Component } from 'react';
import { connect } from 'react-redux';
import ApiService from './../services/api.service'
import {calcBandwithConsume} from './../services/utils.service'
import transaction from './../lib/transaction';
import PropTypes from 'prop-types'
import { SecretKey } from './../constants/crypto';
import CryptoJS from 'crypto-js';
import { keyStorage } from './../constants/localStorage';
import { loadItem } from './../services/storage.service';
import { saveProfileFromApi } from './../actions';
import {Modal,Button} from 'react-materialize'

const mapDispatchToProps = (dispatch) => {
  return{
    saveProfileFromApi: (info) => {dispatch(saveProfileFromApi(info))},
  }
}

const mapStateToProps = (state) => {
  return{
    profile: state.profileReducer.info,
  }
}

class PostBox extends Component {
  static propTypes = {
    profile: PropTypes.object,
    saveProfileFromApi: PropTypes.func
  }

  constructor(props){
    super(props)
    this.apiService = ApiService()
    this.state = {
      content: ''
    }
  }



  handleSubmit(){
    if(this.state.content){
      this.apiService.getCurrentProfile().then((data) => {
        this.props.saveProfileFromApi && this.props.saveProfileFromApi(data)
        const plainTextContent = {
          type: 1,
          text: this.state.content
        }

        let tx = {
          version: 1,
          account: data.public_key,
          sequence: data.sequence + 1,
          memo: Buffer.alloc(0),
          operation: "post",
          params: {
            keys: [],
            content: plainTextContent,
          },
          signature: new Buffer(64)
        }
        let temp = loadItem(keyStorage.private_key)
        let my_private_key = CryptoJS.AES.decrypt(temp, SecretKey).toString(CryptoJS.enc.Utf8)
        transaction.sign(tx, my_private_key);
        let TxEncode = '0x' + transaction.encode(tx).toString('hex');

        const consume = calcBandwithConsume(data, transaction.encode(tx).toString('base64'), new Date());
        if(consume > data.bandwithMax){
          alert('Not enough OXY')
          return;
        }
        else {
          this.apiService.createPost(TxEncode).then((status) => {
            if(status === 'success'){
              this.props.handleAdd(this.state.content, new Date(), transaction.hash(tx))
              this.setState({
                content: ''
              })
            }
            else{
              alert('Post fail')
            }
          })
        }
      })
    }
  }

  handleChangeContent(e){
    this.setState({
      content: e.target.value
    })
  }
  render() {
    return (
        <Modal header='Compose new Post' 
            trigger={<Button waves='light' style={{marginLeft:'220px'}}>Compose New Post</Button>} 
            actions={<Button waves='light' onClick={this.handleSubmit.bind(this)}>Post</Button>}
        >
            <div className="row">
                <div className="input-field col s12">
                    <textarea value={this.state.content} onChange={this.handleChangeContent.bind(this)} className="materialize-textarea areatweet" style={{height:"81px"}} placeholder="What's happening?" ></textarea>
                </div>
            </div>
        </Modal>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostBox);
