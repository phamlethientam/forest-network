import React, { Component,Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { followUser, unFollowUser } from './../actions';
import defaultAvatar from './../img/default-avatar.png'
import { Link } from 'react-router-dom'

const mapStateToProps = (state) => {
  return{
    profile: state.profileReducer.info
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    followUser: (user_id) => {dispatch(followUser(user_id))},
    unFollowUser: (user_id) => {dispatch(unFollowUser(user_id))},
  }
}

class RightSidebarItem extends Component {
  static propTypes = {
    user: PropTypes.object,
    profile: PropTypes.object,
    followUser: PropTypes.func,
    unFollowUser: PropTypes.func,
    onShowConfirm: PropTypes.any,
    isSuccess: PropTypes.bool,
  }

  constructor(props){
    super(props)
    this.state = {
      isFollow: false
    }
  }

  componentDidMount(){
    let temp = this.props.profile.following.find((item) => {
      return item === this.props.user.user_id
    })
    if(temp){
      this.setState({
        isFollow: true
      })
    }
  }
  

  render() {
    return (
      <Fragment>
        <Link to={'/user/' + this.props.user.user_id}>
          <img src={this.props.user.avatar ? this.props.user.avatar : defaultAvatar} alt="avatar" className="circle"></img>
        </Link>
        
        <span className="title">
          <Link to={'/user/' + this.props.user.user_id}>
            <strong>{this.props.user.username}</strong>
          </Link>
          <br/>
          <small style={{wordWrap:'break-word',color:'#76838c'}}>{this.props.user.public_key}</small> 
        </span>
      </Fragment>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RightSidebarItem);
