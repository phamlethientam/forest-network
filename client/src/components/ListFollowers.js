import React, { Component ,Fragment} from 'react';
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import ApiService from './../services/api.service';
import CryptoJS from 'crypto-js';
import { keyStorage } from './../constants/localStorage';
import { loadItem } from './../services/storage.service';
import { SecretKey } from './../constants/crypto';
import base32 from 'base32.js';
import transaction from './../lib/transaction';
import { calcBandwithConsume } from './../services/utils.service';
import { followUser, unFollowUser } from './../actions';
import { Link } from 'react-router-dom';
import {Button} from 'react-materialize'
import defaultAvatar from './../img/default-avatar.png'

const mapDispatchToProps = (dispatch) => {
  return{
    followUser: (user_id) => {dispatch(followUser(user_id))},
    unFollowUser: (user_id) => {dispatch(unFollowUser(user_id))},
  }
}

const mapStateToProps = (state) => {
  return{
    usersFollow: state.profileReducer.usersFollow,
    profile: state.profileReducer.info,
  }
}

class ListFollowers extends Component {
  static propTypes = {
    usersFollow: PropTypes.array,
    followUser: PropTypes.func,
    unFollowUser: PropTypes.func,
    saveProfileFromApi: PropTypes.func,
    idGetListFollow: PropTypes.number
  }

  constructor(props){
    super(props)
    this.apiService = ApiService()
    this.state = {
      users: [],
      page: 1,
      total_page: 0,
      showConfirm: false
    }
  }

  handleShowConfirm(){
    if(!this.state.showConfirm){
      this.setState({
        showConfirm: true
      })
    }
  }

  handleLoadMore(){
    this.apiService.getFollowers(this.props.idGetListFollow, this.state.page, 10).then((data) => {
      data.followers.forEach((item) => {
        let temp = this.props.profile.following.find((findItem) => {
          return findItem === item.user_id
        })
        if(temp){
          item.isFollow = true
        }
        else{
          item.isFollow = false
        }
      })
      this.setState({
        users: [...this.state.users, ...data.followers],
        total_page: data.total_page,
        page: this.state.page + 1
      })
    })
  }

  handleSubmit(){
    let temp = loadItem(keyStorage.private_key)
    let my_private_key = CryptoJS.AES.decrypt(temp, SecretKey).toString(CryptoJS.enc.Utf8)

    this.apiService.getPubkeysFollowing(this.props.profile.public_key, this.props.profile.following).then((data) => {
      let listPubkey = {
        addresses: []
      }

      data.forEach((item) => {
        listPubkey.addresses.push(Buffer.from(base32.decode(item.public_key)))
      })

      this.apiService.getCurrentProfile().then((userProfile) => {
        const profile = userProfile
        const tx = {
          version: 1,
          sequence: profile.sequence + 1,
          memo: Buffer.alloc(0),
          account: profile.public_key,
          operation: "update_account",
          params: {
            key: 'followings',
            value: listPubkey
          },
          signature: new Buffer(64)
        }

        let consume = calcBandwithConsume(profile, transaction.encode(tx).toString('base64'), new Date())
        if(consume > profile.bandwithMax){
            alert('Not enough OXY !')
            return
        }

        transaction.sign(tx, my_private_key);
        let TxEncode = '0x' + transaction.encode(tx).toString('hex');
        this.apiService.updateListFollow(TxEncode).then((flag) => {
            if(flag === 'success'){
                this.setState({
                  showConfirm: false
                })
                alert('Success !')
            }
            else{
                alert('Fail !')
            }
        })
      })
    })
  }

  componentDidMount(){
    this.handleLoadMore()
  }

  handleChangeFollow(item) {
    if(item.isFollow){
      this.props.unFollowUser && this.props.unFollowUser(item.user_id);
    }
    else{
      this.props.followUser && this.props.followUser(item.user_id);
    }
    this.handleShowConfirm()
    let temp = this.state.users.slice()
    for(let i = 0; i < temp.length; i++){
      if(temp[i].user_id === item.user_id){
        temp[i].isFollow = !temp[i].isFollow
        break
      }
    }
    this.setState({
      users: temp
    })
  }

  render() {
    return (
        <Fragment>
            <div className="col m6">
                <ul className="collection" style={{ border: "none" }}>
                    {!!this.state.users.length && this.state.users.map((item, key) => {
                        return(
                            <li className="collection-item avatar" key={key}>
                                <Link to={"/user/" + item.user_id}>
                                    <img  src={item.avatar ? item.avatar : defaultAvatar}alt="" className="circle" />
                                </Link>
                                
                                <span className="title">
                                    <Link to={"/user/" + item.user_id}>
                                        <strong>{item.username}</strong> 
                                    </Link>
                                </span>
                                <span className="right">
                                    {
                                        item.isFollow && item.user_id !== +this.props.idGetListFollow && item.user_id !== this.props.profile.user_id ?
                                        /*eslint-disable-next-line*/
                                        <a  onClick={this.handleChangeFollow.bind(this, item)} className="waves-effect waves-light btn-small btnfollow">
                                            Following
                                        </a>
                                        : !item.isFollow && item.user_id !== +this.props.idGetListFollow && item.user_id !== this.props.profile.user_id ?
                                        /*eslint-disable-next-line*/
                                        <a  onClick={this.handleChangeFollow.bind(this, item)} className="waves-effect waves-light btn-small btnfollow">
                                            Follow
                                        </a>
                                        : null
                                    }
                                </span>
                                <br />
            
                                <p style={{fontSize:'small', color:'#76838c'}}>{item.public_key}</p>
                            </li>
                        )
                        })
                    }
                    {this.state.page <= this.state.total_page &&
                    <li className="collection-item avatar">
                        <Button style={{marginLeft:'200px'}} onClick={this.handleLoadMore.bind(this)} className='blue'> LOAD MORE </Button>
                    </li>
                    }
                </ul>
            </div>
            {
                this.state.showConfirm ?
                /*eslint-disable-next-line*/
                <a onClick={this.handleSubmit.bind(this)}> 
                    <Button floating fab='horizontal' tooltip="confirm" icon='check' className='red' large style={{bottom: '45px', right: '24px'}}>
                    </Button> 
                </a>             
                : 
                null
            }
        </Fragment>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListFollowers);
