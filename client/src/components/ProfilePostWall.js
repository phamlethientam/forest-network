import React, {Component} from 'react';
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import ApiService from './../services/api.service'
import InfiniteScroll from 'react-infinite-scroller'
import PostModal from './PostModal';
import Post from './Post';
import {ProgressBar,Col} from 'react-materialize'

const mapStateToProps = (state) => {
  return {
    profile: state.profileReducer.info
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

class ProfilePostWall extends Component {
  static propTypes = {
    profile: PropTypes.object,
  }

  constructor(props) {
    super(props)
    this.apiService = ApiService()
    this.state = {
      page: 1,
      dataPosts: [],
      dataNewPosts: [],
      hasMoreItems: true
    }
  }

  loadItems(page) {
    this.apiService.getMyPosts(this.props.profile.user_id, this.state.page, 10).then((res) => {
      this.setState({
        dataPosts: [
          ...this.state.dataPosts,
          ...res.posts
        ],
        page: this.state.page + 1,
      }, () => {
        if (this.state.page >= res.total_page) {
          this.setState({hasMoreItems: false})
        }
        else this.setState({hasMoreItems: true})
      })
    })
  }

  handleAddPost(content, createdAt, hash) {
    const newPost = [
      {
        id: this.state.dataNewPosts.length,
        avatar: this.props.profile.avatar,
        user_id: this.props.profile.user_id,
        username: this.props.profile.username,
        created_at: createdAt,
        content: content,
        hash: hash
      }
    ]
    this.setState({
      dataNewPosts: newPost.concat(this.state.dataNewPosts)
    })
  }

  render() {
    const {dataPosts} = this.state
    const newPosts = this.state.dataNewPosts.map(post => {
      const postTemplate = {
        id: post.id,
        avatar: post.avatar,
        user_id: post.user_id,
        username: post.username,
        authorize: "Shared publicly",
        created_on: post.created_at,
        content: post.content,
        hash: post.hash
      }
      return (<Post key={postTemplate.id} post={postTemplate}/>);
    });

    return (
        <div className="col m6">
            <ul className="collection" style={{ border: "none" }}>
                <li className="collection-item">
                    <PostModal handleAdd={this.handleAddPost.bind(this)}/>
                </li>
                {newPosts}
                
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadItems.bind(this)}
                    hasMore={this.state.hasMoreItems}
                    threshold={100}
                    loader={<Col key={0} s={12}> <ProgressBar />  </Col> }>
                    {
                        dataPosts.length > 0
                        ? dataPosts.map((item) => {
                            const postTemplate = {
                            id: item.id,
                            avatar: item.User.avatar,
                            user_id: item.user_id,
                            username: item.User.username,
                            authorize: "Shared publicly",
                            created_on: item.created_at,
                            content: item.content,
                            hash: item.hash
                            }
                            return (<Post key={item.id} post={postTemplate}/>)
                        })
                        :
                        <li className="collection-item avatar">
                            <h2 style={{fontFamily:"'Lobster', cursive",marginLeft:'100px'}}>Sorry No Posts</h2>
                        </li>
                    }
                </InfiniteScroll>
                
            </ul>
        </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePostWall);
