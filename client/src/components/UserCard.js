import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { followUser, unFollowUser } from './../actions';
import { Link } from 'react-router-dom'
import defaultAvatar from './../img/default-avatar.png'

const mapDispatchToProps = (dispatch) => {
  return{
    followUser: (user_id) => {dispatch(followUser(user_id))},
    unFollowUser: (user_id) => {dispatch(unFollowUser(user_id))},
  }
}

const mapStateToProps = (state) => {
  return{
    profile: state.profileReducer.info,
  }
}

class UserCard extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    profile: PropTypes.object,
    followUser: PropTypes.func,
    unFollowUser: PropTypes.func,
    onShowConfirm: PropTypes.func,
  }

  constructor(props){
    super(props)
    this.state = {
      isFollow: false
    }
  }

  componentDidMount(){
    let temp = this.props.profile.following.find((item) => {
      return item === this.props.user.user_id
    })
    if(temp){
      this.setState({
        isFollow: true
      })
    }
  }

  handleFollow(){
    this.setState({
      isFollow: !this.state.isFollow
    }, () =>{
      if(this.state.isFollow){
        this.props.followUser && this.props.followUser(this.props.user.user_id)
      }
      else{
        this.props.followUser && this.props.unFollowUser(this.props.user.user_id)
      }
    })
    this.props.onShowConfirm && this.props.onShowConfirm()
  }

  render() {
    let {user} = this.props
    return (
        <div className="card">
            <div className="card-image">
                <img src="http://gg.gg/clhfc" alt=""/>
            </div>
            <div className="card-content">
                <span className="card-title">
                    <Link to={'/user/' + user.user_id}>
                        <img src={user.avatar ? user.avatar : defaultAvatar} alt="" className="profilecircle" />
                    </Link>
                    <div style={{ marginTop: "-20px" }}>
                    <Link to={'/user/' + user.user_id}>
                        <small style={{color:"#777"}}><b>{user.username}</b></small>
                    </Link>
                    </div>
                </span>
                <small style={{wordWrap:'break-word'}}>{user.public_key} (Public key)</small>
                <div className="row">
                    {this.state.isFollow ?
                        /*eslint-disable-next-line*/
                        <a onClick={this.handleFollow.bind(this)} className="waves-effect waves-light btn-small btnfollow" style={{marginLeft:"55px",marginTop:"10px"}}>
                            Followed
                        </a>
                        :
                        /*eslint-disable-next-line*/
                        <a onClick={this.handleFollow.bind(this)} className="waves-effect waves-light btn-small btnfollow" style={{marginLeft:"55px",marginTop:"10px"}}>
                            Follow
                        </a>
                    }
                </div>
                
            </div>
        </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserCard);
