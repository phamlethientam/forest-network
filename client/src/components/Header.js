import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ApiService from './../services/api.service';
import { keyStorage } from './../constants/localStorage';
import { removeItem } from './../services/storage.service';
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    return{
      profile: state.profileReducer.info
    }
}

class Header extends Component {
    constructor(props){
        super(props)
        this.apiService = ApiService()
        this.state = {
            dropdownStyle: 'none',
            content: ''
        }
    }
    clickDropdown = () => {
        this.setState({
            dropdownStyle: 'block'
        })
    }

    unclickDropdown = () => {
        this.setState({
            dropdownStyle: 'none'
        })
    }

    preventDefault = (e) => {
        e.preventDefault();
    }

    onHandleChange = (e) => {
        this.setState({
            content: e.target.value
        });
    }

    onHandleSubmit = () => {
        if (this.state.content !== '')
        {
            alert(this.state.content)
        }
    }

    handleLogout(){
        this.apiService.logout().then(() => {})
        removeItem(keyStorage.private_key)
    }

    
  render() {
    //let { profile } = this.props
    return (
        <div>
            <div className="navbar-fixed">
                <nav>
                    <div className="nav-wrapper">
                        <Link to="/" className="brand-logo center">
                            <img src="http://gg.gg/clfga" alt=""></img>
                        </Link>
                        {/*eslint-disable-next-line*/}
                        <a onClick={(e) => this.preventDefault(e)} data-target="mobile-demo" className="sidenav-trigger">
                            <i className="material-icons">menu</i>
                        </a>
                        <ul id="nav-mobile" className="left hide-on-med-and-down">
                            <li>
                                <Link to="/">
                                    <i className="material-icons left">home</i>Home
                                </Link>
                            </li>
                            <li>
                                <Link to="/listuser">
                                    <i className="material-icons left">face</i>Find friends
                                </Link>
                            </li>
                        </ul>
                        <ul className="right hide-on-med-and-down">
                            <li>
                                {/*eslint-disable-next-line*/}
                                <a className="dropdown-trigger" onMouseOut={this.unclickDropdown} onMouseOver={this.clickDropdown}>
                                    <i className="material-icons">person_pin</i>
                                </a>
                                <ul id="dropdown1" className="dropdown-content" onMouseOut={this.unclickDropdown} onMouseOver={this.clickDropdown} style={{ display:this.state.dropdownStyle, width: "142.234px", left: "1220px", top: "64px", height: "104px", transformOrigin: "0px 0px 0px", opacity: "1", transform: "scaleX(1) scaleY(1)" }}>
                                    {/* <li className="padding-non-a">
                                        {profile ? profile.username : ''}<br />
                                        <small>{profile ? '@' + profile.public_key : ''}</small>
                                    </li>
                                    <li className="divider" ></li> */}
                                    <li>
                                        <Link to="/profile">
                                            <i className="material-icons left">person</i>Profile
                                        </Link>
                                    </li>                                       
                                    <li className="divider"></li> 
                                    <li>
                                        <Link to="/login" onClick={this.handleLogout.bind(this)}>Log Out</Link>
                                    </li>
                                    <li className="divider"></li> 
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    );
  }
}

export default connect(mapStateToProps)(Header);
