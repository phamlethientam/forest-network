import React, { Component} from 'react';
import { connect } from 'react-redux';
import {Modal,Row,Input,Button} from 'react-materialize'
import PropTypes from 'prop-types';
import { updateProfile, updatePost } from '../actions';
import ApiService from '../services/api.service';
import transaction from '../lib/transaction';
import { calcBandwithConsume } from '../services/utils.service';
import { keyStorage } from '../constants/localStorage';
import { loadItem } from '../services/storage.service';
import { SecretKey } from '../constants/crypto';
import CryptoJS from 'crypto-js';

const mapStateToProps = (state) => {
  return{
    profile: state.profileReducer.info,
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    updateProfile: (info) => {dispatch(updateProfile(info))},
    updatePost: (user) => {dispatch(updatePost(user))},
  }
}

class EditProfile extends Component {
    static propTypes = {
        profile: PropTypes.object,
        updateProfile: PropTypes.func,
        updatePost: PropTypes.func,
        saveProfileFromApi: PropTypes.func
    }

    constructor(props){
        super(props)
        this.apiService = ApiService()
        this.limitSize = 30000
        this.state = {
            originImageUrl: '',
            imagePreviewUrl: '',
            username: '',
            file_avatar: null
        }
    }

    componentDidMount(){
        this.setState({
          originImageUrl: this.props.profile.avatar
        })
    }

    preventDefault = (e) => {
        e.preventDefault();
    }

    handleOnSelectFileAvatar(event) {
        let file = event.target.files[0]
        let reader = new FileReader();
        reader.onloadend = () => {
          this.setState({
            file_avatar: file,
            imagePreviewUrl: reader.result
          });
        }
        reader.readAsDataURL(file)
    }

    handleChangeUsername(e){
        this.setState({
          username: e.target.value
        })
    }

    handleSaveEdit(){
        if(this.state.username === '')
        {
          alert('Empty username !')
          return
        }
        if(this.state.file_avatar && this.state.file_avatar.size > this.limitSize)
        {
          alert('Invalid img! Use another img')
          return
        }
    
        if(this.state.file_avatar){
          let reader = new FileReader()
          let encodedImage = null
          reader.onload = () => {
            encodedImage = new Buffer(reader.result, 'binary');
            this.handleSubmit(encodedImage)
          }
          reader.readAsBinaryString(this.state.file_avatar)
        }else{
          this.handleSubmit(null)
        }
      }
    
      handleSubmit(bufferImage) {
        const data = {}
        let dateImage = null
        let flagSendImage = false
        let changedImage = this.state.imagePreviewUrl
        let extraOXY = null
    
        let temp = loadItem(keyStorage.private_key)
        let my_private_key = CryptoJS.AES.decrypt(temp, SecretKey).toString(CryptoJS.enc.Utf8)


        this.apiService.getCurrentProfile().then((userProfile) => {
          const profile = userProfile
          if(bufferImage){
            const tx = {
              version: 1,
              sequence: profile.sequence + 1,
              memo: Buffer.alloc(0),
              account: profile.public_key,
              operation: "update_account",
              params: {
                key: 'picture',
                value: bufferImage
              },
              signature: new Buffer(64)
            }
            flagSendImage = true
            dateImage = new Date()
    
            transaction.sign(tx, my_private_key);
            let TxEncode = '0x' + transaction.encode(tx).toString('hex');
            extraOXY = calcBandwithConsume(profile, transaction.encode(tx).toString('base64'), dateImage) > profile.bandwithMax
            if(extraOXY){
              alert('Not enough OXY')
              return
            }
            data.hexImage = TxEncode
          }
    
          if(this.state.username !== profile.username){
            const tx = {
              version: 1,
              sequence: flagSendImage ? profile.sequence + 2 : profile.sequence + 1,
              memo: Buffer.alloc(0),
              account: profile.public_key,
              operation: "update_account",
              params: {
                key: 'name',
                value: new Buffer(this.state.username)
              },
              signature: new Buffer(64)
            }
            transaction.sign(tx, my_private_key);
            let TxEncode = '0x' + transaction.encode(tx).toString('hex');
            if(flagSendImage){
              if(calcBandwithConsume(profile, transaction.encode(tx).toString('base64'), dateImage, extraOXY) > profile.bandwithMax){
                alert('Not enough OXY')
                return
              }
            }
            else{
              if(calcBandwithConsume(profile, transaction.encode(tx).toString('base64'), new Date()) > profile.bandwithMax){
                alert('Not enough OXY')
                return
              }
            }
            data.hexUsername = TxEncode
          }
    
          if(Object.keys(data).length){
            this.apiService.updateProfile(data).then((res) => {
              let isFailedAll = false
              let countFailed = 0
              res.forEach((bool) => {
                if(bool.value === 'failed'){
                  countFailed += 1
                }
              })
              if(countFailed === res.length){
                isFailedAll = true
            }
    
            if(isFailedAll){
                alert('Update failed!')
                this.setState({
                  username: profile.username
                })
                return
            }
    
            alert('Update success!')

    
            this.props.updateProfile && this.props.updateProfile({
                username: this.state.username,
                avatar: changedImage ? changedImage : this.state.originImageUrl
            })
    
            this.props.updatePost && this.props.updatePost({
                id: this.props.profile.user_id,
                username: this.state.username
              })
            })
          }

        this.setState({
            imagePreviewUrl: ''
        })

    
        })
    }

  render() {
    return (
        <Modal fixedFooter header='Edit Profile' 
                trigger={
                  /*eslint-disable-next-line*/
                    <a  className="waves-effect waves-light btn-small">
                        <span style={{fontFamily : "'Playfair Display', serif",color: "white"} }>Edit Profile</span>
                    </a>
                }
                actions={
                    <Button waves='light' onClick={this.handleSaveEdit.bind(this)}>Update</Button>
                } 
        >
            <div className="row">
                <div className="input-field col s12">
                    <i className="material-icons prefix">
                        account_circle
                    </i>
                    <input id="icon_prefix" value={this.state.username} onChange={this.handleChangeUsername.bind(this)} type="text" className="validate" />
                        <label htmlFor="icon_prefix">New Username</label>
                </div>
                <div className="input-field col s12">
                    <Row>
                        <Input type="file" onChange={this.handleOnSelectFileAvatar.bind(this)} label="New Avatar" s={12} />
                    </Row>
                </div>
                
                <div className="input-field col s12">
                <img alt="" width="150" style={{marginLeft:'250px'}} height="150" className="circle photoprofile" id="target" src={this.state.imagePreviewUrl}/>
                </div>
            </div>         
        </Modal>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
