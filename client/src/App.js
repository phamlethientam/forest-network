import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom'
import  { PrivateRoute } from './components'
import Home from './pages/Home'
import Login from './pages/Login'
import ErrorPage from './pages/ErrorPage'
import Profile from './pages/Profile'
import Others from './pages/Others'
import ListUser from './pages/ListUser'
import TransactionHistory from './pages/TransactionHistory'

class App extends Component {
  componentDidMount(){
    document.addEventListener("DOMContentLoaded", function(event) {
      window.scrollTo(0, 0)
    });
  }
  render() {
    return (
      <Router>
          <Switch>
            <Redirect from='/home' to='/' />
            <PrivateRoute exact path='/' component={Home} />
            <Route exact path='/login' component={Login} />
            <PrivateRoute exact path='/profile' component={Profile} />
            <PrivateRoute exact path='/listuser' component={ListUser} />
            <PrivateRoute exact path='/user/:id' component={Others} />
            <PrivateRoute exact path='/transaction-history' component={TransactionHistory} />
            <Route component={ErrorPage} />
          </Switch>
      </Router>
    );
  }
}

export default App;
